<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ErrorFormRequest extends FormRequest
{

    public function authorize()
    {

        return true;
    }


    public function rules()
    {
        return [
            // validasi
            'name' => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required|min:8',
        ];
    }

    public function messages()
    {
        return
        [
            'name.required' => 'Nama Tidak Boleh Kosong',
            'email.required' => 'Email Tidak Boleh Kosong',
            'email.unique' => 'Email Tidak Boleh Sama',
            'password.required' => 'password tidak boleh kosong!',
            'password.min' => 'password harus 8 caracter!',
        ];
    }
}
