<?php

namespace App\Http\Controllers;

use App\Models\Angka;
use Illuminate\Http\Request;

class AngkaTerbilangController extends Controller
{
    //

    public function index()
    {
        $angka = Angka::paginate(10);
        return view('angka-terbilang.index', compact('angka'));
    }
}
