<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\ErrorFormRequest;

class ManagementUsersController extends Controller
{

    public function index()
    {
        // menampilkan data
        $user = User::all();
        return view('users.management-users', compact('user'));
    }

    public function addpost()
    {
        $response = Http::post('https://jsonplaceholder.typicode.com/posts', [
            'userId' => 1,
            'title' => 'Dendy',
            'body' => 'Dendy Prasetyo',
        ]);

        return $response->json();
    }


    public function create()
    {
        // menampilkan view
        return view('users.tambah-users');
    }

    public function store(ErrorFormRequest $request)
    {

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'remember_token' => Str::random(60)
        ]);

        return redirect('/management-users')->with('success', 'data berhasil disimpan');
    }

    public function show($id)
    {
        //
        $data = User::find($id);
        return view('users.detail', compact('data'));
    }


    public function edit($id)
    {
        // mengubah data berdasarkan id
        $data = User::find($id);
        return view('users.edit', compact('data'));
    }


    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
         ],
        [
            'name.required' => 'Nama tidak boleh kosong',
            'email.rquired' => 'Email tidak boleh kosong',
            'password.rquired' => 'Password tidak boleh kosong',
        ]);

        $data = User::find($id);
        $data->name = $request->name;
        $data->email = $request->email;
        $data->password = $request->password;
        $data->save();
        return redirect('/management-users');
    }


    public function destroy($id)
    {
        // menghapus data berdasarkan id
        $user = User::find($id);
        $user->delete();
        return redirect('/management-users')->with('danger', 'data berhasil dihapus');
    }
}
