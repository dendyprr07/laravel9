<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ErrorFormRequest;

class LoginController extends Controller
{
    //
    public function halamanlogin(){
        return view('login.login');
    }

    public function postlogin(Request $request) {
        if(Auth::attempt($request->only('email', 'password'))){
            return redirect('/home');
        }
        return redirect('/');
    }

    public function registrasi() {
        return view('login.registrasi');
    }

    public function simpanregistrasi(ErrorFormRequest $request)
    {
        // dd($request->all());

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'remember_token' => Str::random(60)
        ]);

        return view('login.login');

    }

    public function logout()
    {
        Auth::logout();
        return redirect('/home');
    }
}
