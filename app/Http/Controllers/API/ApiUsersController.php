<?php

namespace App\Http\Controllers\API;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;

class ApiUsersController extends Controller
{

    public function index()
    {
        //menampilkan api menggunakan laravel
        $user = User::all();
        return response()->json([
            'status' => true,
            'message' => 'List semua data users',
            'data' => $user
        ]);

    }

    public function get()
    {
        $response = Http::get('http://149.129.221.143/kanaldata/Webservice/bank_account');

        return $response->json();
    }

}
