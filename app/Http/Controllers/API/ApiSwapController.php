<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApiSwapController extends Controller
{
    //

    public function swap()
    {
        $A = 5;
        $B = 3;

        $swap = list($A, $B) = [$B, $A];

        return response()->json([
            'status' => true,
            'Message' => 'Data berhasil ditukar',
            'data' => $swap
        ]);
    }
}
