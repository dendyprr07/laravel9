<?php

use App\Http\Controllers\API\ApiProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\ApiUsersController;
use App\Http\Controllers\API\ProductStockController;
use App\Http\Controllers\API\ApiProductStockController;
use App\Http\Controllers\API\ApiSwapController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});



Route::get('/api-users', [ApiUsersController::class, 'index']);
Route::get('/api-get', [ApiUsersController::class, 'get']);
Route::get('/api-swap', [ApiSwapController::class, 'swap']);
Route::get('/api-product', [ApiProductController::class, 'post']);
