<?php

use App\Http\Controllers\AngkaTerbilangController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ManagementUsersController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductStockController;
use App\Http\Controllers\SwapController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/registrasi', [LoginController::class, 'registrasi'])->name('registrasi');
Route::post('/simpanregistrasi', [LoginController::class, 'simpanregistrasi'])->name('simpanregistrasi');
Route::get('/login', [LoginController::class, 'halamanlogin'])->name('login');
Route::post('/postlogin', [LoginController::class, 'postlogin'])->name('postlogin');

Route::middleware(['auth'])->group(function () {
    Route::get('/home', [HomeController::class, 'index'])->name('home');

    // crud management Users
    Route::get('/management-users', [ManagementUsersController::class, 'index'])->name('management-users');
    Route::get('/tambah-users', [ManagementUsersController::class, 'create'])->name('tambah-users');
    Route::post('/store', [ManagementUsersController::class, 'store'])->name('store');
    Route::get('/edit/{id}', [ManagementUsersController::class, 'edit'])->name('edit');
    Route::put('/update/{id}', [ManagementUsersController::class, 'update'])->name('update');
    Route::get('/delete/{id}', [ManagementUsersController::class, 'destroy'])->name('destroy');
    Route::get('/detail/{id}', [ManagementUsersController::class, 'show'])->name('show');
    // Route::post('/update/{id}', [ManagementUsersController::class, 'update'])->name('update');

    // menampilkan halaman users, lalu akan muncul api di dalam halaman users
    Route::get('/datausers', [UsersController::class, 'index'])->name('data-users');

    // menampilkan halaman product, lalu akan muncul api di dalam halaman product
    Route::get('/product', [ProductController::class, 'index'])->name('index-product');

    // menampilkan halaman angka terbilang, lalu akan muncul api di dalam halaman angka terbilang
    Route::get('/angka-terbilang', [AngkaTerbilangController::class, 'index'])->name('angka-terbilang');

    // menampilkan halaman swap, lalu akan muncul api di dalam halaman swap
    Route::get('/data-swap', [SwapController::class, 'index'])->name('swap-index');
});

Route::get('/logout', [LoginController::class, 'logout'])->name('logout');
