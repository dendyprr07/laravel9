<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('home') }}" class="brand-link">
      <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ auth()->user()->name }}</a>
        </div>
      </div>

      <!-- SidebarSearch Form -->

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="{{ route('management-users') }}" class="nav-link">
              <p>
                <p>Manajement User</p>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('index-product') }}" class="nav-link">
              <p>
                <p>Product Stock</p>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('data-users') }}" class="nav-link">
              <p>
                <p>Data Api User</p>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('angka-terbilang') }}" class="nav-link">
              <p>
                <p>Angka Terbilang</p>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('swap-index') }}" class="nav-link">
              <p>
                <p>Swap Angka</p>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('logout') }}" class="nav-link">
              <p>
                <p>Logout</p>
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
