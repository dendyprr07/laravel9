<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class AngkaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker  = Faker::create();

        for($i = 1; $i <=  1000000000; $i++) {
            DB::table('angkas')->insert([
                'angka' => $faker->numberBetween(1,1000000000),
                'created_at' => Carbon::now()
            ]);
        }
    }
}
